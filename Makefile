.PHONY: up, down, down-v, restart
up:
	@docker-compose up -d
up-b:
	@docker-compose up -d --build
down:
	@docker-compose down
down-v:
	@docker-compose down --volumes
restart:
	@docker-compose down && docker-compose up -d
restart-b:
	@docker-compose down && docker-compose up -d --build
restart-v:
	@docker-compose down -v && docker-compose up -d
restart-vb:
	@docker-compose down -v && docker-compose up -d --build

